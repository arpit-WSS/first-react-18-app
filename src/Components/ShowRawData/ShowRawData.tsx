import { Card, CardContent, Divider, Grid, Typography, Button } from '@mui/material';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { rawData, Item } from '../TableData/TableData';

export const ShowRawData = () => {
	const { objectID } = useParams();
	const navigate = useNavigate();

	console.log(rawData[0]);

	const handlePosts = (e: React.MouseEvent<HTMLButtonElement>) => {
		navigate('/posts');
	};
	const handleHome = (e: React.MouseEvent<HTMLButtonElement>) => {
		navigate('/');
		window.location.reload();
		localStorage.clear();
	};

	return (
		<Grid container item md={12} justifyContent="center" alignItems="center" direction="column">
			<Card sx={{ maxWidth: '900px', background: '#d8e2dc', marginTop: '20px' }}>
				<CardContent>
					{rawData ? (
						rawData.map((item: any) =>
							item.map((post: Item, index: number) => {
								if (post.objectID === objectID) {
									return (
										<React.Fragment key={index}>
											<Typography variant="h5" sx={{ color: 'red' }}>
												ObjectID: {post.objectID}
											</Typography>
											<Divider />
											<Typography variant="body1">{JSON.stringify(post)}</Typography>
										</React.Fragment>
									);
								}
							})
						)
					) : null}
				</CardContent>
			</Card>
			<Grid item sx={{ marginTop: '30px' }}>
				<Button variant="contained" onClick={handlePosts}>
					Go to Posts
				</Button>
				<Button variant="contained" onClick={handleHome}>
					Go Home
				</Button>
			</Grid>
		</Grid>
	);
};

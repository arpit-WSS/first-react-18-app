import { Grid, Table, TableContainer, TableHead, Paper, TableRow, TableCell, TableBody, Link } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';

export const dataStore: Item[] = [];
export const rawData: any[] = [];
let pagenum = 1;

export type Item = {
	title: 'string';
	author: 'string';
	url: 'string';
	objectID: 'string';
	created_at: 'string';
};

export const fetchData = async (page: number): Promise<any> => {
	const endpoint = `https://hn.algolia.com/api/v1/search_by_date?tags=story&page=${page}`;
	const resp = await fetch(endpoint);
	const dataPosts = await resp.json();
	return dataPosts;
};

export const TableData = () => {
	const [ data, setdata ] = useState([]);
	const [ loading, setloading ] = useState(false);
	const [ error, seterror ] = useState(false);
	const refreshvalue: boolean = JSON.parse(localStorage.getItem('refreshValue')!);

	useEffect(() => {
		(async () => {
			try {
				setloading(true);
				if (refreshvalue == true) {
					const getData = await fetchData(0);

					setdata(getData.hits);
					rawData.push(getData.hits);
					getData.hits.map((post: any) =>
						dataStore.push({
							title: post.title,
							author: post.author,
							url: post.url,
							objectID: post.objectID,
							created_at: post.created_at
						})
					);
				}
			} catch (error) {
				seterror(true);
				setloading(false);
			}
		})();

		setloading(false);
		localStorage.setItem('refreshValue', JSON.stringify(false));
	}, []);

	useEffect(
		() => {
			const interval = setInterval(async () => {
				const getData = await fetchData(pagenum++);
				getData.hits.map((item: Item) =>
					dataStore.push({
						title: item.title,
						url: item.url,
						author: item.author,
						created_at: item.created_at,
						objectID: item.objectID
					})
				);
				setdata(getData.hits);
				rawData.push(getData.hits);
			}, 10000);
			return () => clearInterval(interval);
		},
		[ pagenum ]
	);

	return (
		<Grid container item md={12} justifyContent="center" alignItems="center">
			<TableContainer component={Paper} sx={{ maxWidth: '1400px' }}>
				<Table>
					<TableHead>
						<TableRow sx={{ background: 'black' }}>
							<TableCell sx={{ color: 'white' }}>Title</TableCell>
							<TableCell sx={{ color: 'white' }}>URL</TableCell>
							<TableCell sx={{ color: 'white' }}>Author</TableCell>
							<TableCell sx={{ color: 'white' }}>Created At</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{dataStore ? (
							dataStore.map((post: Item, index: number) => (
								<TableRow key={index}>
									<TableCell>
										<Link component={RouterLink} to={`./${post.objectID}`}>
											{post.title}
										</Link>
									</TableCell>
									<TableCell>
										<Link component={RouterLink} to={`./${post.objectID}`}>
											{post.url}
										</Link>
									</TableCell>
									<TableCell>
										<Link component={RouterLink} to={`./${post.objectID}`}>
											{post.author}
										</Link>
									</TableCell>
									<TableCell>
										<Link component={RouterLink} to={`./${post.objectID}`}>
											{post.created_at}
										</Link>
									</TableCell>
								</TableRow>
							))
						) : null}
					</TableBody>
				</Table>
			</TableContainer>
		</Grid>
	);
};

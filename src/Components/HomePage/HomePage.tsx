import { Button, Grid } from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';
export const HomePage = () => {
	const navigate = useNavigate();

	const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		navigate('/posts');
		localStorage.setItem('refreshValue', JSON.stringify(true));
	};
	return (
		<Grid container item md={12} justifyContent="center" alignItems="center" sx={{ minHeight: '695px' }}>
			<Button variant="contained" onClick={handleClick}>
				Fetch Post
			</Button>
		</Grid>
	);
};

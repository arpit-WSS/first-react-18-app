import React from 'react';

import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AppBarComponent } from './Components/AppBarComponent/AppBarComponent';
import { HomePage } from './Components/HomePage/HomePage';
import { TableData } from './Components/TableData/TableData';
import { ShowRawData } from './Components/ShowRawData/ShowRawData';

function App() {
	return (
		<BrowserRouter>
			<AppBarComponent />
			<Routes>
				<Route path="/" element={<HomePage />} />
				<Route path="/posts" element={<TableData />} />
				<Route path="/posts/:objectID" element={<ShowRawData />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;

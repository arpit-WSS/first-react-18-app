import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import React from 'react';

export const AppBarComponent = () => {
	return (
		<Box>
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="h6" align="center" sx={{ width: '100%' }}>
						FetchPost App
					</Typography>
				</Toolbar>
			</AppBar>
		</Box>
	);
};

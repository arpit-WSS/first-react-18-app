import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { HomePage } from '../HomePage';

describe('', () => {
	it('TEST-1', () => {
		render(<HomePage />, { wrapper: BrowserRouter });
		const button = screen.getByText(/Fetch Post/i);
		expect(button).toBeInTheDocument();
	});

	it('TEST-2', () => {
		render(<HomePage />, { wrapper: BrowserRouter });
		const button = screen.getByText(/Fetch Post/i);
		fireEvent.click(button);
	});
});

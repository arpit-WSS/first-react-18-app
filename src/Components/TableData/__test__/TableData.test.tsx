import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import { TableData, dataStore, Item } from '../TableData';
import {} from '../TableData';

const mockData = {
	title: 'Happy World',
	url: 'https://indiaisbest.com',
	author: 'SundarLal',
	created_at: '04/04/2022',
	objectID: '10005'
};

dataStore.push(mockData as Item);

describe('Test TableData', () => {
	it('Test-1', () => {
		function tree() {
			return render(
				<BrowserRouter>
					<TableData />
				</BrowserRouter>
			);
		}

		const { getByText } = tree();

		const textElem = getByText(/SundarLal/i);
		expect(textElem).toBeInTheDocument();
	});
});

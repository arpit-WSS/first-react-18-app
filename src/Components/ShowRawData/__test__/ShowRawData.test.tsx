import { render, screen, fireEvent } from '@testing-library/react';
import { BrowserRouter, useParams } from 'react-router-dom';
import { ShowRawData } from '../ShowRawData';
import { rawData } from '../../TableData/TableData';

const mockData = [
	{
		title: 'Happy World',
		url: 'https://indiaisbest.com',
		author: 'SundarLal',
		created_at: '04/04/2022',
		objectID: '10005'
	}
];

rawData.push(mockData);

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useParams: () => ({
		objectID: '10005'
	})
}));

describe('Test component', () => {
	it('Test-1', () => {
		render(
			<BrowserRouter>
				<ShowRawData />
			</BrowserRouter>
		);
		const button = screen.getByText(/Go to Posts/i);
		expect(button).toBeInTheDocument();
	});

	it('Test-2', () => {
		render(
			<BrowserRouter>
				<ShowRawData />
			</BrowserRouter>
		);
		const button = screen.getByText(/Go to Posts/i);
		fireEvent.click(button);
	});

	it('Test-3', () => {
		function tree() {
			return render(
				<BrowserRouter>
					<ShowRawData />
				</BrowserRouter>
			);
		}

		const { getByText } = tree();
		const { objectID } = useParams();

		const textElem = getByText(/ObjectID: 10005/i);
		expect(textElem).toBeInTheDocument();
	});
});
